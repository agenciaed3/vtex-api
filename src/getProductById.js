import searchProduct from './searchProduct';
import { utilify } from '@ed3digital/utils/utilify';
const globalHelpers = utilify.globalHelpers;
/**
 * @module product
 */

const getProductsById = async (id) => {
  let fq = '';
  if (globalHelpers.isArray(id)) {
    fq = id.map((product) => `productId:${product}`).join(',');
  } else {
    fq = `productId:${id}`;
  }
  return searchProduct({ fq });
};
export default getProductsById;
