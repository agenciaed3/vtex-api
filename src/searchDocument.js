import parseFileds from './internal/parseFields';

import masterdataRequest from './services/masterdataRequest';

const searchDocument = ({ search, entity, fields, filters, offset = 0, limit = 49, auth, accountName, }) => {
  // Needs refactor to _from/_to args
  const headers = [`REST-Range: resources=${offset}-${limit + offset}`];
  const args = Object.assign(Object.assign(Object.assign({}, search), filters), { _fields: parseFileds(fields) });
  return masterdataRequest({
      entity,
      type: 'search',
      method: 'GET',
      data: args,
      headers,
      auth,
      accountName,
  });
};
export default searchDocument;