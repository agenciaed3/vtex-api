import request from '../request';
import createCatalogUrl from '../internal/createCatalogUrl';
import createHeaders from '../internal/createHeaders';
import createAuthentication from '../internal/createAuthentication';
import isLocalhost from '../@ed3digital/utils/isLocalhost';
/**
 * @module catalog-system
 */
const catalogRequest = ({ path, query, headers, accountName, auth }) => {
  const authentication = createAuthentication(auth);
  const url = isLocalhost
    ? 'http://localhost:3000/products'
    : createCatalogUrl({
        path,
        query,
        accountName,
      });
  const defaults = [];
  const config = {
    headers: new Headers(
      createHeaders(
        defaults
          .concat(headers || [])
          .concat(authentication || [])
          .filter(Boolean)
      )
    ),
  };
  return request(url, config);
};
export default catalogRequest;
