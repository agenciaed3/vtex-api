import catalogRequest from './services/catalogRequest';
/**
 * @module catalog-system
 */
const getCategories = ({ treeLevel, query, headers, accountName, auth, }) => (catalogRequest({
    path: `/category/tree/${treeLevel}`,
    query,
    headers,
    accountName,
    auth,
}));
export default getCategories;

