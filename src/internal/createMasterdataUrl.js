import encode from '../@ed3digital/utils/encode';
import hostname from './hostname';

const createMasterdataUrl = ({
  entity,
  type,
  id,
  method,
  data,
  accountName,
}) => {
  const mountedUrl = `/api/dataentities/${entity}/${type}/${id || ''}`;
  const mountedData = data ? `?${encode(data)}` : '';
  const hasAn = data && Object.prototype.hasOwnProperty.call(data, 'an');
  const url =
    method === 'GET' || hasAn ? `${mountedUrl}${mountedData}` : mountedUrl;
  return url;
};
export default createMasterdataUrl;
