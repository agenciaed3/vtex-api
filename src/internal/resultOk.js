import head from '../@ed3digital/utils/head';

const resultOk = (result) =>
  !!(result !== undefined && result.length && head(result).id !== undefined);
export default resultOk;
