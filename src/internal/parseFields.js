import uniq from '../@ed3digital/utils/uniq';

const parseFileds = (fields) => {
  if (!Array.isArray(fields) || !fields.length) return 'id';
  fields.push('id');
  return uniq(fields).join(',');
};
export default parseFileds;
