import hostname from './hostname';
import trimSlashes from '../@ed3digital/utils/trimSlashes';
import clearQuery from '../@ed3digital/utils/clearQuery';

/**
 * @module catalog-system
 */

const createCatalogUrl = ({ path, query }) => {
  console.log(path, query);
  const cleanQuery = query ? `?${clearQuery(query)}` : '';
  return `/api/catalog_system/pub/${trimSlashes(path)}${cleanQuery}`;
};
export default createCatalogUrl;
