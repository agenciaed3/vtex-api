import trim from '../@ed3digital/utils/trim';

const hostname = (accountName) =>
  accountName ? trim(`https://${accountName}.vtexcommercestable.com.br`) : '';

export default hostname;
