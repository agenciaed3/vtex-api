import catalogRequest from './services/catalogRequest';
/**
 * @module catalog-system
 */
const getCategoryById = ({ id, query, headers, accountName, auth, }) => (catalogRequest({
    path: `/category/${id}`,
    query,
    headers,
    accountName,
    auth,
}));
export default getCategoryById;