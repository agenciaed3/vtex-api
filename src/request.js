const parseJSON = async (response) => {
  const { status, statusText, headers } = response;
  if (status === 204 || status === 205) {
      return {
          status, statusText, headers, json: null,
      };
  }
  const json = await response.json();
  return {
      status, statusText, headers, json,
  };
};

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
      return response;
  }
  const error = new Error(response.statusText);
  Object.assign(error, { response });
  throw error;
};




const request = (url, options) => fetch(url, options)
    .then(checkStatus)
    .then(parseJSON);
    
export default request;