import request from './request';
import trim from '@ed3digital/utils/trim';
import isLocalhost from '@ed3digital/utils/isLocalhost';

const searchAutocomplete = ({ searchTerm, maxRows = 10 }) =>
  request(
    isLocalhost
      ? `http://localhost:3000/searchAutoComplete?${encodeURIComponent(
          trim(searchTerm)
        )}`
      : `
    /buscaautocomplete?maxRows=${maxRows}&productNameContains=${encodeURIComponent(
          trim(searchTerm)
        )}
  `
  );
export default searchAutocomplete;
