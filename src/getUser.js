import isEmail from '@ed3digital/utils/isEmail';

import searchDocument from './searchDocument';

/**
 * Get User object from Entity Masterdata CL
 *
 * @category Validate
 * @param {string} email
 * @param {string} fields
 * @param {object} auth
 * @param {string} accountName
 *
 * @module isEmail
 * @module searchDocument
 *
 * @return {object}
 */

const getUser = ({ email, fields, auth, accountName }) => {
  if (!isEmail(email)) return Promise.reject(new Error('Invalid email'));
  return searchDocument({
    search: { email },
    fields,
    entity: 'CL',
    offset: 0,
    limit: 1,
    auth,
    accountName,
  });
};
export default getUser;
