export { default as request } from './request';

/** General */
export { default as getCategories } from './getCategories';
export { default as getFacetsCategory } from './getFacetsCategory';
export { default as getCategoryById } from './getCategoryById';
export { default as searchAutocomplete } from './searchAutocomplete';
export { default as searchProduct } from './searchProduct';

/** Masterdata methods */
export { default as masterdataRequest } from './services/masterdataRequest';
export { default as partialUpdate } from './services/partialUpdate';
export { default as searchDocument } from './searchDocument';
export { default as insertDocument } from './insertDocument';
export { default as updateDocument } from './updateDocument';
export { default as getDocument } from './getDocument';
export { default as getUser } from './getUser';
export { default as updateUser } from './updateUser';
export { default as newsletterOptIn } from './newsletterOptIn';

/** Product */
export { default as notifyMe } from './notifyMe';
//export { default as simulateShipping } from './simulateShipping';
export { default as getProductById } from './getProductById';