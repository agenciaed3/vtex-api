import facetsCategoryRequest from './services/facetsCategoryRequest';
/**
 * @module catalog-system
 */
const getFacetsCategory = ({ pathname, query, headers, accountName, auth, }) => (facetsCategoryRequest({
    path: `/facets/search${pathname}`,
    query,
    headers,
    accountName,
    auth,
}));
export default getFacetsCategory;