import request from './request';
var SearchCrossSellingTypes;
(function (SearchCrossSellingTypes) {
    SearchCrossSellingTypes["whoboughtalsobought"] = "whoboughtalsobought";
    SearchCrossSellingTypes["similars"] = "similars";
    SearchCrossSellingTypes["whosawalsosaw"] = "whosawalsosaw";
    SearchCrossSellingTypes["whosawalsobought"] = "whosawalsobought";
    SearchCrossSellingTypes["accessories"] = "accessories";
    SearchCrossSellingTypes["suggestions"] = "suggestions";
})(SearchCrossSellingTypes || (SearchCrossSellingTypes = {}));
const searchCrossSelling = ({ id, type, }) => (request(`/api/catalog_system/pub/products/crossselling/${type}/${id}`));
export default searchCrossSelling;