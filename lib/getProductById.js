"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _searchProduct = _interopRequireDefault(require("./searchProduct"));

var _utilify = require("@ed3digital/utils/utilify");

var globalHelpers = _utilify.utilify.globalHelpers;
/**
 * @module product
 */

var getProductsById = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee(id) {
    var fq;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            fq = '';

            if (globalHelpers.isArray(id)) {
              fq = id.map(function (product) {
                return "productId:".concat(product);
              }).join(',');
            } else {
              fq = "productId:".concat(id);
            }

            return _context.abrupt("return", (0, _searchProduct.default)({
              fq: fq
            }));

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getProductsById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _default = getProductsById;
exports.default = _default;