"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createMasterdataUrl = _interopRequireDefault(require("../internal/createMasterdataUrl"));

var _createHeaders = _interopRequireDefault(require("../internal/createHeaders"));

var _createAuthentication = _interopRequireDefault(require("../internal/createAuthentication"));

var _request = _interopRequireDefault(require("../request"));

var masterdataRequest = function masterdataRequest(_ref) {
  var entity = _ref.entity,
      type = _ref.type,
      id = _ref.id,
      method = _ref.method,
      data = _ref.data,
      headers = _ref.headers,
      accountName = _ref.accountName,
      auth = _ref.auth;
  var authentication = (0, _createAuthentication.default)(auth);
  var url = (0, _createMasterdataUrl.default)({
    entity: entity,
    type: type,
    id: id,
    method: method,
    data: data,
    accountName: accountName
  });
  var defaults = ['Accept: application/vnd.vtex.ds.v10+json', 'Content-Type: application/json; charset=utf-8'];
  var config = {
    method: method,
    body: method !== 'GET' ? JSON.stringify(data) : null,
    headers: new Headers((0, _createHeaders.default)(defaults.concat(headers || []).concat(authentication || []).filter(Boolean)))
  };
  return (0, _request.default)(url, config);
};

var _default = masterdataRequest;
exports.default = _default;