"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _request = _interopRequireDefault(require("../request"));

var _createCatalogUrl = _interopRequireDefault(require("../internal/createCatalogUrl"));

var _createHeaders = _interopRequireDefault(require("../internal/createHeaders"));

var _createAuthentication = _interopRequireDefault(require("../internal/createAuthentication"));

var _isLocalhost = _interopRequireDefault(require("../@ed3digital/utils/isLocalhost"));

/**
 * @module catalog-system
 */
var catalogRequest = function catalogRequest(_ref) {
  var path = _ref.path,
      query = _ref.query,
      headers = _ref.headers,
      accountName = _ref.accountName,
      auth = _ref.auth;
  var authentication = (0, _createAuthentication.default)(auth);
  var url = _isLocalhost.default ? 'http://localhost:3000/facets' : (0, _createCatalogUrl.default)({
    path: path,
    query: query,
    accountName: accountName
  });
  var defaults = [];
  var config = {
    headers: new Headers((0, _createHeaders.default)(defaults.concat(headers || []).concat(authentication || []).filter(Boolean)))
  };
  return (0, _request.default)(url, config);
};

var _default = catalogRequest;
exports.default = _default;