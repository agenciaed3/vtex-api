"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _masterdataRequest = _interopRequireDefault(require("./masterdataRequest"));

/**
 * Partial update of a document
 *
 * @param {string} id     The ID of the document to update
 * @param {object} data   The data that will be updated
 * @param {string} entity The entity of the document to insert
 *
 * @module masterdata
 *
 * @example
 *  const response = await partialUpdate({
 *    id: 'aa65fd51-0dab-11ea-82ee-9e3712d56bb3',
 *    data: {
 *      firstName: 'Edited',
 *      lastName: 'Edited',
 *      ...
 *    },
 *    entity: 'CL',
 *  });
 *
 * @return {promise}
 */
var partialUpdate = function partialUpdate(_ref) {
  var id = _ref.id,
      data = _ref.data,
      entity = _ref.entity,
      auth = _ref.auth,
      accountName = _ref.accountName;
  return (0, _masterdataRequest.default)({
    method: 'PATCH',
    id: id,
    data: data,
    entity: entity,
    type: 'documents',
    auth: auth,
    accountName: accountName
  });
};

var _default = partialUpdate;
exports.default = _default;