"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _request = _interopRequireDefault(require("./request"));

var notifyMe = function notifyMe(_ref) {
  var name = _ref.name,
      email = _ref.email,
      itemId = _ref.itemId;
  var formData = new FormData();
  formData.append('notifymeClientName', name);
  formData.append('notifymeClientEmail', email);
  formData.append('notifymeIdSku', "".concat(itemId));
  return (0, _request.default)('/no-cache/AviseMe.aspx', {
    method: 'POST',
    body: formData
  });
};

var _default = notifyMe;
exports.default = _default;