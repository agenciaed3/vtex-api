"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var parseJSON = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee(response) {
    var status, statusText, headers, json;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            status = response.status, statusText = response.statusText, headers = response.headers;

            if (!(status === 204 || status === 205)) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return", {
              status: status,
              statusText: statusText,
              headers: headers,
              json: null
            });

          case 3:
            _context.next = 5;
            return response.json();

          case 5:
            json = _context.sent;
            return _context.abrupt("return", {
              status: status,
              statusText: statusText,
              headers: headers,
              json: json
            });

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function parseJSON(_x) {
    return _ref.apply(this, arguments);
  };
}();

var checkStatus = function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  var error = new Error(response.statusText);
  Object.assign(error, {
    response: response
  });
  throw error;
};

var request = function request(url, options) {
  return fetch(url, options).then(checkStatus).then(parseJSON);
};

var _default = request;
exports.default = _default;