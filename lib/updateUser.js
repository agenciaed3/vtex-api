"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _head = _interopRequireDefault(require("@ed3digital/utils/head"));

var _resultOk = _interopRequireDefault(require("./internal/resultOk"));

var _partialUpdate = _interopRequireDefault(require("./services/partialUpdate"));

var _getUser = _interopRequireDefault(require("./getUser"));

var _insertDocument = _interopRequireDefault(require("./insertDocument"));

/**
 * Update a user if the email exists, or insert a new one if it doesn't
 *
 * @param {string} email The email of the user
 * @param {object} data  The data that will be updated
 *
 * @module masterdata
 *
 * @example
 *  const response = await updateUser({
 *    email: 'john@doe.com',
 *    data: {
 *      firstName: 'John',
 *      lastName: 'Doe',
 *      ...
 *    },
 *  });
 *
 * @return {promise}
 */
var updateUser = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee(_ref2) {
    var email, data, auth, accountName, _yield$getUser, json;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            email = _ref2.email, data = _ref2.data, auth = _ref2.auth, accountName = _ref2.accountName;
            _context.next = 3;
            return (0, _getUser.default)({
              email: email,
              fields: ['id']
            });

          case 3:
            _yield$getUser = _context.sent;
            json = _yield$getUser.json;
            return _context.abrupt("return", (0, _resultOk.default)(json) ? (0, _partialUpdate.default)({
              id: (0, _head.default)(json).id,
              data: data,
              entity: 'CL',
              auth: auth,
              accountName: accountName
            }) : (0, _insertDocument.default)({
              data: Object.assign({
                email: email
              }, data),
              entity: 'CL',
              auth: auth,
              accountName: accountName
            }));

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function updateUser(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _default = updateUser;
exports.default = _default;