"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _partialUpdate = _interopRequireDefault(require("./services/partialUpdate"));

/**
 * Insert/update a document
 *
 * @param {string} id     The ID of the item that will be inserted/updated
 * @param {object} data   The data that will be inserted
 * @param {string} entity The entity of the document to insert
 *
 * @module masterdata
 *
 * @example
 *  const response = await updateDocument({
 *    id: 'aa65fd51-0dab-11ea-82ee-9e3712d56bb3',
 *    data: {
 *      firstName: 'Edited',
 *      lastName: 'Edited',
 *      ...
 *    },
 *    entity: 'CL',
 *  });
 *
 * @return {promise}
 */
var updateDocument = function updateDocument(_ref) {
  var id = _ref.id,
      data = _ref.data,
      entity = _ref.entity,
      auth = _ref.auth,
      accountName = _ref.accountName;
  return (0, _partialUpdate.default)({
    id: id,
    data: data,
    entity: entity,
    auth: auth,
    accountName: accountName
  });
};

var _default = updateDocument;
exports.default = _default;