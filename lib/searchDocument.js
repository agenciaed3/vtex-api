"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _parseFields = _interopRequireDefault(require("./internal/parseFields"));

var _masterdataRequest = _interopRequireDefault(require("./services/masterdataRequest"));

var searchDocument = function searchDocument(_ref) {
  var search = _ref.search,
      entity = _ref.entity,
      fields = _ref.fields,
      filters = _ref.filters,
      _ref$offset = _ref.offset,
      offset = _ref$offset === void 0 ? 0 : _ref$offset,
      _ref$limit = _ref.limit,
      limit = _ref$limit === void 0 ? 49 : _ref$limit,
      auth = _ref.auth,
      accountName = _ref.accountName;
  // Needs refactor to _from/_to args
  var headers = ["REST-Range: resources=".concat(offset, "-").concat(limit + offset)];
  var args = Object.assign(Object.assign(Object.assign({}, search), filters), {
    _fields: (0, _parseFields.default)(fields)
  });
  return (0, _masterdataRequest.default)({
    entity: entity,
    type: 'search',
    method: 'GET',
    data: args,
    headers: headers,
    auth: auth,
    accountName: accountName
  });
};

var _default = searchDocument;
exports.default = _default;