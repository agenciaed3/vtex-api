"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _updateUser = _interopRequireDefault(require("./updateUser"));

/**
 * Newsletter opt-in / opt-out
 *
 * @param {string} email            The email of the user to opt-in/out
 * @param {boolean} [optIn=true]    Whether to opt-in/out
 * @param {object} [data=undefined] Custom data to send
 *
 * @module masterdata
 *
 * @example
 *  const response = await newsletterOptIn({
 *    email: 'john@doe.com',
 *    data: {
 *      firstName: 'John Doe',
 *      ...
 *    },
 *  });
 *
 * @return {promise}
 */
var newsletterOptIn = function newsletterOptIn(_ref) {
  var email = _ref.email,
      _ref$optIn = _ref.optIn,
      optIn = _ref$optIn === void 0 ? true : _ref$optIn,
      data = _ref.data,
      auth = _ref.auth,
      accountName = _ref.accountName;
  return (0, _updateUser.default)({
    email: email,
    data: Object.assign({
      isNewsletterOptIn: optIn
    }, data),
    auth: auth,
    accountName: accountName
  });
};

var _default = newsletterOptIn;
exports.default = _default;