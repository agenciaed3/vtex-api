"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _uniq = _interopRequireDefault(require("../@ed3digital/utils/uniq"));

var parseFileds = function parseFileds(fields) {
  if (!Array.isArray(fields) || !fields.length) return 'id';
  fields.push('id');
  return (0, _uniq.default)(fields).join(',');
};

var _default = parseFileds;
exports.default = _default;