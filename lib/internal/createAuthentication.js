"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var createAuthentication = function createAuthentication(auth) {
  if (!auth) return [];
  var appKey = auth.appKey,
      appToken = auth.appToken;
  return ["x-vtex-api-appKey: ".concat(appKey), "x-vtex-api-appToken: ".concat(appToken)];
};

var _default = createAuthentication;
exports.default = _default;