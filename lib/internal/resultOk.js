"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _head = _interopRequireDefault(require("../@ed3digital/utils/head"));

var resultOk = function resultOk(result) {
  return !!(result !== undefined && result.length && (0, _head.default)(result).id !== undefined);
};

var _default = resultOk;
exports.default = _default;