"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _hostname = _interopRequireDefault(require("./hostname"));

var _trimSlashes = _interopRequireDefault(require("../@ed3digital/utils/trimSlashes"));

var _clearQuery = _interopRequireDefault(require("../@ed3digital/utils/clearQuery"));

/**
 * @module catalog-system
 */
var createCatalogUrl = function createCatalogUrl(_ref) {
  var path = _ref.path,
      query = _ref.query;
  console.log(path, query);
  var cleanQuery = query ? "?".concat((0, _clearQuery.default)(query)) : '';
  return "/api/catalog_system/pub/".concat((0, _trimSlashes.default)(path)).concat(cleanQuery);
};

var _default = createCatalogUrl;
exports.default = _default;