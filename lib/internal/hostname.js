"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _trim = _interopRequireDefault(require("../@ed3digital/utils/trim"));

var hostname = function hostname(accountName) {
  return accountName ? (0, _trim.default)("https://".concat(accountName, ".vtexcommercestable.com.br")) : '';
};

var _default = hostname;
exports.default = _default;