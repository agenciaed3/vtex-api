"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _trim = _interopRequireDefault(require("../@ed3digital/utils/trim"));

var createHeaders = function createHeaders(headers) {
  if (!headers) return {};
  var result = {};

  for (var i = 0, len = headers.length; i < len; i += 1) {
    var row = headers[i];
    var index = row.indexOf(':');
    var key = (0, _trim.default)(row.slice(0, index));
    var value = (0, _trim.default)(row.slice(index + 1));

    if (typeof result[key] === 'undefined') {
      result[key] = value;
    } else if (Array.isArray(result[key])) {
      result[key].push(value);
    } else {
      result[key] = [result[key], value];
    }
  }

  return result;
};

var _default = createHeaders;
exports.default = _default;