"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _encode = _interopRequireDefault(require("../@ed3digital/utils/encode"));

var _hostname = _interopRequireDefault(require("./hostname"));

var createMasterdataUrl = function createMasterdataUrl(_ref) {
  var entity = _ref.entity,
      type = _ref.type,
      id = _ref.id,
      method = _ref.method,
      data = _ref.data,
      accountName = _ref.accountName;
  var mountedUrl = "/api/dataentities/".concat(entity, "/").concat(type, "/").concat(id || '');
  var mountedData = data ? "?".concat((0, _encode.default)(data)) : '';
  var hasAn = data && Object.prototype.hasOwnProperty.call(data, 'an');
  var url = method === 'GET' || hasAn ? "".concat(mountedUrl).concat(mountedData) : mountedUrl;
  return url;
};

var _default = createMasterdataUrl;
exports.default = _default;