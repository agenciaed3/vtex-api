"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _masterdataRequest = _interopRequireDefault(require("./services/masterdataRequest"));

/**
 * Insert a document
 *
 * @param {object} data - The data that will be inserted
 * @param {string} entity - The entity of the document to insert
 *
 * @module masterdata
 *
 * @example
 *  const response = await insertDocument({
 *    data: {
 *      firstName: 'Foo',
 *      lastName: 'Bar',
 *      email: 'foo@bar.com',
 *      ...
 *    },
 *    entity: 'CL',
 *  });
 *
 * @return {promise}
 */
var insertDocument = function insertDocument(_ref) {
  var data = _ref.data,
      entity = _ref.entity,
      auth = _ref.auth,
      accountName = _ref.accountName;
  return (0, _masterdataRequest.default)({
    method: 'POST',
    data: data,
    entity: entity,
    type: 'documents',
    auth: auth,
    accountName: accountName
  });
};

var _default = insertDocument;
exports.default = _default;