"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _parseFields = _interopRequireDefault(require("./internal/parseFields"));

var _masterdataRequest = _interopRequireDefault(require("./services/masterdataRequest"));

/**
 * Get document by Id
 *
 * @param {string} id     Document ID
 * @param {array} fields  The Fields that will be retrieved
 * @param {string} entity The entity where the search will be performed
 *
 * @module masterdata
 *
 * @example
 *  const response = await getDocument({
 *    id: 'aa65fd51-0dab-11ea-82ee-9e3712d56bb3',
 *    fields: ['_all'],
 *    entity: 'CL',
 *  });
 *
 * @return {promise}
 */
var getDocument = function getDocument(_ref) {
  var id = _ref.id,
      fields = _ref.fields,
      entity = _ref.entity,
      auth = _ref.auth,
      accountName = _ref.accountName;
  return (0, _masterdataRequest.default)({
    method: 'GET',
    id: id,
    data: {
      _fields: (0, _parseFields.default)(fields)
    },
    entity: entity,
    type: 'documents',
    auth: auth,
    accountName: accountName
  });
};

var _default = getDocument;
exports.default = _default;