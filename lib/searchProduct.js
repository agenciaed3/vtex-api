"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _catalogRequest = _interopRequireDefault(require("./services/catalogRequest"));

var _trim = _interopRequireDefault(require("@ed3digital/utils/trim"));

/**
 * @module catalog-system
 */
var searchProduct = function searchProduct(_ref) {
  var ft = _ref.ft,
      fq = _ref.fq,
      orderBy = _ref.orderBy,
      from = _ref.from,
      to = _ref.to,
      priceRange = _ref.priceRange,
      collection = _ref.collection,
      salesChannel = _ref.salesChannel,
      seller = _ref.seller,
      headers = _ref.headers,
      accountName = _ref.accountName,
      auth = _ref.auth;
  var query = '';
  if (ft) query += "&ft=".concat((0, _trim.default)(ft)); // eslint-disable-next-line prefer-template

  if (fq) query += '&' + (Array.isArray(fq) ? fq.map(function (val) {
    return "fq=".concat(val);
  }).join('&') : "fq=".concat(fq));
  if (orderBy) query += "&O=".concat(orderBy);
  if (from !== undefined && from > -1) query += "&_from=".concat(from);
  if (to !== undefined && to > -1) query += "&_to=".concat(to);
  if (priceRange) query += "&fq=P:[".concat(priceRange, "]");
  if (collection) query += "&fq=productClusterIds:".concat(collection);
  if (salesChannel) query += "&fq=isAvailablePerSalesChannel_".concat(salesChannel, ":1");
  if (seller) query += "&fq=sellerIds:".concat(seller);
  if (query.charAt(0) === '&') query = query.substr(1);
  return (0, _catalogRequest.default)({
    path: '/products/search',
    query: query,
    headers: headers,
    accountName: accountName,
    auth: auth
  });
};

var _default = searchProduct;
exports.default = _default;