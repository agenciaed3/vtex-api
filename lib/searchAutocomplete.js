"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _request = _interopRequireDefault(require("./request"));

var _trim = _interopRequireDefault(require("@ed3digital/utils/trim"));

var _isLocalhost = _interopRequireDefault(require("@ed3digital/utils/isLocalhost"));

var searchAutocomplete = function searchAutocomplete(_ref) {
  var searchTerm = _ref.searchTerm,
      _ref$maxRows = _ref.maxRows,
      maxRows = _ref$maxRows === void 0 ? 10 : _ref$maxRows;
  return (0, _request.default)(_isLocalhost.default ? "http://localhost:3000/searchAutoComplete?".concat(encodeURIComponent((0, _trim.default)(searchTerm))) : "\n    /buscaautocomplete?maxRows=".concat(maxRows, "&productNameContains=").concat(encodeURIComponent((0, _trim.default)(searchTerm)), "\n  "));
};

var _default = searchAutocomplete;
exports.default = _default;