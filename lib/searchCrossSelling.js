"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _request = _interopRequireDefault(require("./request"));

var SearchCrossSellingTypes;

(function (SearchCrossSellingTypes) {
  SearchCrossSellingTypes["whoboughtalsobought"] = "whoboughtalsobought";
  SearchCrossSellingTypes["similars"] = "similars";
  SearchCrossSellingTypes["whosawalsosaw"] = "whosawalsosaw";
  SearchCrossSellingTypes["whosawalsobought"] = "whosawalsobought";
  SearchCrossSellingTypes["accessories"] = "accessories";
  SearchCrossSellingTypes["suggestions"] = "suggestions";
})(SearchCrossSellingTypes || (SearchCrossSellingTypes = {}));

var searchCrossSelling = function searchCrossSelling(_ref) {
  var id = _ref.id,
      type = _ref.type;
  return (0, _request.default)("/api/catalog_system/pub/products/crossselling/".concat(type, "/").concat(id));
};

var _default = searchCrossSelling;
exports.default = _default;