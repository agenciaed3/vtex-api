"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _facetsCategoryRequest = _interopRequireDefault(require("./services/facetsCategoryRequest"));

/**
 * @module catalog-system
 */
var getFacetsCategory = function getFacetsCategory(_ref) {
  var pathname = _ref.pathname,
      query = _ref.query,
      headers = _ref.headers,
      accountName = _ref.accountName,
      auth = _ref.auth;
  return (0, _facetsCategoryRequest.default)({
    path: "/facets/search".concat(pathname),
    query: query,
    headers: headers,
    accountName: accountName,
    auth: auth
  });
};

var _default = getFacetsCategory;
exports.default = _default;