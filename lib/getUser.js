"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmail = _interopRequireDefault(require("@ed3digital/utils/isEmail"));

var _searchDocument = _interopRequireDefault(require("./searchDocument"));

/**
 * Get User object from Entity Masterdata CL
 *
 * @category Validate
 * @param {string} email
 * @param {string} fields
 * @param {object} auth
 * @param {string} accountName
 *
 * @module isEmail
 * @module searchDocument
 *
 * @return {object}
 */
var getUser = function getUser(_ref) {
  var email = _ref.email,
      fields = _ref.fields,
      auth = _ref.auth,
      accountName = _ref.accountName;
  if (!(0, _isEmail.default)(email)) return Promise.reject(new Error('Invalid email'));
  return (0, _searchDocument.default)({
    search: {
      email: email
    },
    fields: fields,
    entity: 'CL',
    offset: 0,
    limit: 1,
    auth: auth,
    accountName: accountName
  });
};

var _default = getUser;
exports.default = _default;