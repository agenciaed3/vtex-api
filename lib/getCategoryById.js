"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _catalogRequest = _interopRequireDefault(require("./services/catalogRequest"));

/**
 * @module catalog-system
 */
var getCategoryById = function getCategoryById(_ref) {
  var id = _ref.id,
      query = _ref.query,
      headers = _ref.headers,
      accountName = _ref.accountName,
      auth = _ref.auth;
  return (0, _catalogRequest.default)({
    path: "/category/".concat(id),
    query: query,
    headers: headers,
    accountName: accountName,
    auth: auth
  });
};

var _default = getCategoryById;
exports.default = _default;